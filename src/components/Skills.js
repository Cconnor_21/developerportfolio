import React, {Component} from 'react';
import {Jumbotron} from 'reactstrap';
//importing images
import csharp from '../skillsimages/csharp.png';
import html5 from '../skillsimages/html.png';
import css3 from '../skillsimages/css.png';
import php from '../skillsimages/php.png';
import javascript from '../skillsimages/javascript.png';
import react from '../skillsimages/react.png';
import yii from '../skillsimages/Yii.png';
import asp from '../skillsimages/asp.png';
import express from '../skillsimages/express.png';
import mysql from '../skillsimages/mysql.png';
import mongo from '../skillsimages/mongodb.png';
import gitlab from '../skillsimages/gitlab.png';
import github from '../skillsimages/github.png';
import aws from '../skillsimages/aws.png';
import digitalocean from '../skillsimages/digitalocean.png';
import mlab from '../skillsimages/mlab.png';
import python from '../skillsimages/python.png';
import rubyonrails from '../skillsimages/rubyonrails.png';
import scala from '../skillsimages/scala.png';
import java from '../skillsimages/java.png';
import sass from '../skillsimages/sass.png';
import bootstrap from '../skillsimages/bootstrap.png';
import jquery from '../skillsimages/jquery.png';

class Skills extends Component{
  render(){
    return(
      <div style={{display:'flex', justifyContent:'center',alignItems:'center'}}>
        <Jumbotron className="jumbo" style={{borderRadius:'0px'}}>
        <h1 style={{textAlign:'center'}}>My Skills</h1>
            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Programming Languages</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={csharp} /></div>
              <div className="skill"><img className="skillsImage" src={html5} /></div>
              <div className="skill"><img className="skillsImage" src={css3} /></div>
              <div className="skill"><img className="skillsImage" src={php} /></div>
              <div className="skill"><img className="skillsImage" src={javascript} /></div>
            </div>

            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Frameworks</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={react} /></div>
              <div className="skill"><img className="skillsImage" src={yii} /></div>
            </div>

            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Databases</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={mysql} /></div>
              <div className="skill"><img className="skillsImage" src={mongo} /></div>
            </div>

            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Version Control</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={gitlab} /></div>
              <div className="skill"><img className="skillsImage" src={github} /></div>
            </div>

            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Cloud Services</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={aws} /></div>
              <div className="skill"><img className="skillsImage" src={digitalocean} /></div>
              <div className="skill"><img className="skillsImage" src={mlab} /></div>
            </div>

            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Other</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={jquery} /></div>
              <div className="skill"><img className="skillsImage" src={express} /></div>
              <div className="skill"><img className="skillsImage" src={bootstrap} /></div>
            </div>

            <h2 style={{textAlign:'center', margin:'20px',fontSize:'25px'}}>Exposure to</h2>
            <div className="skillsSection">
              <div className="skill"><img className="skillsImage" src={asp} /></div>
              <div className="skill"><img className="skillsImage" src={python} /></div>
              <div className="skill"><img className="skillsImage" src={rubyonrails} /></div>
              <div className="skill"><img className="skillsImage" src={scala} /></div>
              <div className="skill"><img className="skillsImage" src={java} /></div>
              <div className="skill"><img className="skillsImage" src={sass} /></div>
            </div>
        </Jumbotron>
      </div>
    );
  }
}

export default Skills;
