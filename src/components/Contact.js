import React, {Component} from 'react';
import {Jumbotron} from 'reactstrap';

class Contact extends Component{
  render(){
    return(
      <div style={{display:'flex', justifyContent:'center',alignItems:'center'}}>
        <Jumbotron className="jumbo" style={{borderRadius:'0px'}}>
        <div style={{overflowWrap: 'break-word'}}>
          <h1 style={{textAlign:'center', marginBottom: '30px'}}>Contact Me</h1>
          <h2 style={{textAlign:'center'}}><i className="fa fa-envelope"></i> connor.gdesign@gmail.com</h2>
          <h2 style={{textAlign:'center'}}><i className="fa fa-phone"></i> 541-650-1748</h2>
          </div>
        </Jumbotron>
      </div>
    );
  }
}

export default Contact;
