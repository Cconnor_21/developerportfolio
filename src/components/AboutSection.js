import React, {Component} from 'react';
import {Jumbotron} from 'reactstrap';
import profile from '../profile.jpg';

class AboutSection extends Component{
  render(){
    return(
      <div style={{display:'flex', justifyContent:'center',alignItems:'center'}}>
        <Jumbotron className="jumbo" style={{borderRadius:'0px'}}>

          <div style={{display:'flex', width:'100%', justifyContent:'center'}}>
            <div className="profileBorder" style={{position:'relative', padding:'7px', borderRadius:'100%'}}>
            <div style={{position:'relative', width:'220px', height:'220px', overflow:'hidden', borderRadius: '50%',border:'4px solid white'}}>
                <img style={{width:'100%', height: 'auto', marginTop:'-10px'}} src={profile} alt="profilepicture" />
            </div>
            </div>
          </div>
          <h1 style={{textAlign:'center', fontSize:'60px'}}>Cody Connor</h1>
          <h2 style={{textAlign:'center'}}>Software Developer</h2>
          <div style={{width:'100%', display:'flex', justifyContent:'center', marginTop:'20px'}}>
            <p>Hello, My name is Cody. I live in Springfield Oregon and I have a Computer Programming - Associate of Applied Science Degree from Lane Community College. I am currently looking for a full time development job. I enjoy learning new programming technologies and expanding my knowledge with the ones I already know.</p>
          </div>

          <div style={{width:'100%', display:'flex', justifyContent:'center', alignItems:'center'}}>

            <a href="https://gitlab.com/Cconnor_21"><svg aria-hidden="true" className="gitlab"  focusable="false" data-prefix="fab" data-icon="gitlab"  role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M105.2 24.9c-3.1-8.9-15.7-8.9-18.9 0L29.8 199.7h132c-.1 0-56.6-174.8-56.6-174.8zM.9 287.7c-2.6 8 .3 16.9 7.1 22l247.9 184-226.2-294zm160.8-88l94.3 294 94.3-294zm349.4 88l-28.8-88-226.3 294 247.9-184c6.9-5.1 9.7-14 7.2-22zM425.7 24.9c-3.1-8.9-15.7-8.9-18.9 0l-56.6 174.8h132z"></path></svg></a>

            <a href="https://www.linkedin.com/in/cody-connor-431239185/"><svg aria-hidden="true"className="linkedin" focusable="false" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z"></path></svg></a>

          </div>
        </Jumbotron>
      </div>
    );
  }
}

export default AboutSection;
