import React, {Component} from 'react';
import {Jumbotron} from 'reactstrap';
import weatherapp from '../weatherapp.png';
import mygrocerylistapp from '../mygrocerylistapp.png';
import toptenmusiccharts from '../toptenmusiccharts.png';
import reactweather from '../reactweather.png';
import todolist from '../TodoList.png';
import capstone from '../capstone.png';
import guitarcharts from '../guitarchordcharts.png';

class Projects extends Component{
  render(){
    return(
      <div style={{display:'flex', justifyContent:'center',alignItems:'center'}}>
        <Jumbotron className="jumbo" style={{borderRadius:'0px'}}>
        <h1 style={{textAlign:'center', marginBottom:'30px'}}>Some of my personal projects</h1>
          <div style={{display:'flex', width:'100%', justifyContent:'center', alignItems:'center', flexWrap:'wrap'}}>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>

          <div className="projectBox">
            <div className="projectTitle">GuitarChordCharts (React.js) Current Project
              </div>
            <div className="imageContainer">
              <img className="image"  src={guitarcharts} />
            </div>
            <div style={{width:'100%',height:'55px',backgroundColor:'white',display:'flex',justifyContent:'center',alignItems:'center'
            }}>
              <a href="https://gitlab.com/Cconnor_21/guitarchordfinder" className="customButton">View on Gitlab</a>
            </div>
          </div>




          </div>
        </Jumbotron>
      </div>
    );
  }
}

export default Projects;
