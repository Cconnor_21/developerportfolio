import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

class MainNavbar extends Component{
  constructor(props){
    super(props);

    this.toggleMenu = this.toggleMenu.bind(this);
    this.state = {
      menuClass: 'collapse navbar-collapse'
    }
  }


  render(){
    return(
      <nav className="navbar navbar-expand-md bg-primary navbar-dark">
      <a className="navbar-brand" href="#">CODY CONNOR</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span className="navbar-toggler-icon"></span>
      </button>
      <div className={this.state.menuClass} id="collapsibleNavbar">
      <ul className="navbar-nav mx-auto">
        <li className="nav-item">
          <NavLink exact to={'/'} onClick={this.toggleMenu} className="nav-link" activeClassName="selected">ABOUT</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to={'/skills'} onClick={this.toggleMenu} className="nav-link" activeClassName="selected">SKILLS</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to={'/projects'} onClick={this.toggleMenu} className="nav-link" activeClassName="selected">PERSONAL PROJECTS</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to={'/contact'} className="nav-link" activeClassName="selected">CONTACT</NavLink>
        </li>
    </ul>
  </div>
</nav>
    );
  }
}

export default MainNavbar;
