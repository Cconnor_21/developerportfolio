import React, {Component} from 'react';
import './App.css';
import './output.css';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import MainNavbar from './components/MainNavbar';
import AboutSection from './components/AboutSection';
import Skills from './components/Skills';
import Projects from './components/Projects';
import Contact from './components/Contact';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class App extends Component {
  render(){
    return (
      <React.Fragment>
        <Router>
        <MainNavbar />
          <Route path='/' exact component={AboutSection} />
          <Route path='/projects' component={Projects} />
          <Route path='/skills' component={Skills} />
          <Route path='/contact' component={Contact} />
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
